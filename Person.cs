using System;

namespace ex_28
{
    public class Person
    {
        private string Name;
        private int Age;

        public Person(string _name, int _age)
        {
            Name = _name;
            Age = _age;
        }
        public void SayHello()
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine($"My name is {Name} and my age is {Age}");
        }
    }
}
